import Individual


class Population(object):
    """A representation of the population."""

    def __init__(self):
        self.size = 0
        self.individuals = None

    def initialize_population(self, individuals: [Individual]):
        self.individuals = individuals
        self.size = len(self.individuals)

    def get_individuals(self):
        return self.individuals

    def set_individuals(self, individuals: []):
        self.individuals = None
        self.individuals = individuals

    def get_ranked_individuals(self):
        return sorted(self.individuals, key=lambda x: x.fitness, reverse=False)

    def rank_population(self):
        self.individuals.sort(key=lambda x: x.fitness, reverse=False)

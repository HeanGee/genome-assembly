import random

import Helpers


class Individual(object):
    """
    Representation of an individual.

    :param fitness: The fitness value.
    :param chromosome: An array representing his chromosome.
    :param crossover_method: The crossover point cut.
    """

    def __init__(self, fitness: int, chromosome: [], crossover_method: str):
        self.fitness = fitness
        self.chromosome = chromosome
        if crossover_method == 'half':
            self.crossover_point = round(len(chromosome) / 2)

    def __str__(self):
        genes = ' '.join(str(self.chromosome))
        return str(self.fitness) + ': ' + genes

    @staticmethod
    def generate_random_chromosome():
        return random.sample(range(10000), 5)

    def get_fitness(self):
        return self.fitness

    def set_fitness(self, fitness: int):
        self.fitness = fitness

    def get_chromosome(self):
        return self.chromosome

    def set_chromosome(self, chromosome: []):
        self.chromosome = chromosome

    def crossover(self, individual: 'Individual'):
        """
        Returns 2 childrens by half cutting crossover.

        :type individual: Individual
        :param individual: The partner with which make crossover.
        :return: Two Individuals instance representing 2 childrens.
        :rtype: Individual
        """

        mother = self.chromosome[:]
        father = individual.chromosome[:]  # like array.copy() but without incurring to the context change

        chromosome_1 = mother[:self.crossover_point]
        chromosome_2 = father[:self.crossover_point]

        while True:
            new_genes1 = Helpers.diff_genes(father, chromosome_1)  # list(set(father) - set(chromosome_1))
            new_genes2 = Helpers.diff_genes(mother, chromosome_2)  # list(set(mother) - set(chromosome_2))

            if len(new_genes1) == 0:  # or len(new_genes2) is also valid.
                break

            chromosome_1.append(new_genes1.pop(0))
            chromosome_2.append(new_genes2.pop(0))

            if len(chromosome_1) == len(mother):  # or len(father) is also valid.
                break

        children_1 = Individual(0, chromosome_1, 'half')
        children_2 = Individual(0, chromosome_2, 'half')

        children_1.__mutate()
        children_2.__mutate()

        return children_1, children_2

    def __mutate(self):
        """
        Apply mutation to the current individual selecting arbitrary 2 genes from his chromosome.

        :return:
        """
        a, b = random.randint(0, len(self.chromosome) - 1), random.randint(0, len(self.chromosome) - 1)
        self.chromosome[a], self.chromosome[b] = self.chromosome[b], self.chromosome[a]

import Individual
import Population


class Environment(object):
    pop_size = 10

    def __init__(self, generations):
        individuals = []
        fitness = 0
        crossover_method = 'half'
        self.generations = generations

        for i in range(self.pop_size):
            # fitness = random.randint(1, 55)
            chromosome = Individual.Individual.generate_random_chromosome()
            individuals.append(Individual.Individual(fitness, chromosome, crossover_method))

        self.population = Population.Population()
        self.population.initialize_population(individuals)
        self.evaluate_population()

    def evaluate_population(self):
        for i in range(len(self.population.individuals)):
            # Here the fitness is directly the sum of the values of his genes.
            # Could be more elaborate evaluating the values of his genes.
            self.population.individuals[i].set_fitness(sum(self.population.individuals[i].get_chromosome()))
        self.population.rank_population()

    def time_lapse(self):
        for i in range(self.generations):
            _ = self.population.individuals.pop(0)
            _ = self.population.individuals.pop(0)
            mother = self.population.individuals[len(self.population.individuals)-1]
            father = self.population.individuals[len(self.population.individuals)-2]
            child1, child2 = mother.crossover(father)
            self.population.individuals.append(child1)
            self.population.individuals.append(child2)
            self.evaluate_population()

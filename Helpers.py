def diff_genes(first, second):
    """
    Calculate list difference.

    Return a new list containing all values in first that is not cointained in second.

    :param first:
    :param second:
    :return:
    """
    second = set(second)
    return [item for item in first if item not in second]
